% Function that performs Nested Cross Validation on the data produced by
% the "data_for_clsf".

% Inputs:
%       time_evolution_counts: rearranged data, ready to be fed to a classifier
%                              ("neurons"-features x trials-samples) - spikes
%                              counts in each time window
%       labels: vector of labels of condition variation for every trial
%       time_step: amount of time points that the windows slides (in msec)
%       zero: time point when cue is displayed (in msec)
%       num_folds: number of folds of the nested cv
%       kernel_type: type of kernel used for the SVM classifier
%                    (linear / rbf)

% Outputs:
%       mean_accuracies: mean accuracy of the model per time step
%       num_feat: number of features used for training in each time step

function [mean_accuracies, num_feat] = nested_CV(time_evolution_counts,...
                                        labels, time_step, zero, num_folds,...
                                        kernel_type)

    % Initialization of arrays that store the accuracies mean and sem of
    % each rime step (outer loop).
    mean_accuracies = zeros(1, size(time_evolution_counts,3));
    
    % Initialization of array that stores the number of features used in
    % each time step.
    num_feat = zeros(1, size(time_evolution_counts,3));
    
    % Outer loop: select appropriate data matrix at each time step
    for i=1:size(time_evolution_counts,3)
        % Retrieve the variables and target output of the samples in each
        % fold.
        pattern = time_evolution_counts(:,:,i);

        % If there exist neurons in any time point that have only zeros
        % across all trials, throw them out.
% 	     to_keep = ~all(pattern == 0, 2);
% 	     pattern = pattern(to_keep,:);

		% If there exist neurons in any time point that have small spike
        % count (across all trials) after cue presentation, throw them out.
%        curr_time = 1 + time_step*(i-1);
%        if curr_time >= zero
        to_keep = ~all(pattern < 10, 2);
        pattern = pattern(to_keep,:);
%        end

        % Select the number of features to be fed to the classifier and
        % save this number for future reference.
        nFeatures = size(pattern,1);
        num_feat(1,i) = nFeatures;

        % Inner loop: CV for parameter selection and accuracy estimation -
        % use "cross_val".
        [mean_accuracy] = cross_val(pattern, labels, num_folds, kernel_type);

        mean_accuracies(1,i) = mean_accuracy;
       
    end
            
end