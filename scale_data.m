% Function that scales inputs in the specified range [a, b].

% Inputs:
%       data: data whose features are scaled in the specified range
%       range: (1 x 2) vector specifying the scaling
%       mm_scale: min-max scaling of training data, passed to test data,
%                 not passed to training data

% Outputs:
%       data_sc: scaled data
%       mm_scale: min-max scaling of training data, passed to test data

function [data_sc, mm_scale] = scale_data(data, range, mm_scale)

    % Get values from 'range'.
    a = range(1);
    b = range(2);
    
    if ~exist('mm_scale', 'var')
        % Find min and max values per training feature (column).
        minima = min(data, [], 1);
        maxima = max(data, [], 1);
        mm_scale = {minima, maxima};
    else
        % Retrieve min and max values per training feature.
        minima = mm_scale{1};
        maxima = mm_scale{2};
    end

    % Min-max scaling.
    num = (b - a) .* (data - repmat(minima, size(data, 1), 1));
    den = repmat(maxima, size(data, 1), 1) - repmat(minima, size(data, 1), 1);
    
    data_sc = num ./ den + a;

end