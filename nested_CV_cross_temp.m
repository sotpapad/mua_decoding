% Function that performs Nested Cross Validation on the data produced by
% the "data_for_clsf", but evaluates every time window against all others.

% Inputs:
%       time_evolution_counts: rearranged data, ready to be fed to a classifier
%                              ("neurons"-features x trials-samples) - spikes
%                              counts in each time window
%       labels: vector of labels of condition variation for every trial
%       time_step: amount of time points that the windows slides (in msec)
%       zero: time point when cue is displayed (in msec)
%       num_folds: number of folds of the nested cv
%       kernel_type: type of kernel used for the SVM classifier
%                    (linear / rbf)

% Outputs:
%       mean_accuracies: mean accuracy of the model per time step
%       num_feat: number of features used for training in each time step

function [mean_accuracies, num_feat] = nested_CV_cross_temp(time_evolution_counts,...
                                        labels, time_step, zero, num_folds,...
                                        kernel_type)
									
    % Initialization of arrays that store the accuracies mean and sem of
    % each time step (outer loop).
    mean_accuracies = zeros(size(time_evolution_counts,3), size(time_evolution_counts,3));
    
    % Initialization of array that stores the number of features used in
    % each time step.
    num_feat = zeros(size(time_evolution_counts,3), size(time_evolution_counts,3));

	% Select each time window as training data.
	for traintime = 1:size(time_evolution_counts,3)
		
		% Outer loop: select appropriate data matrix at each time step.
		
		% Select each time window as test data.
		for testtime = 1:size(time_evolution_counts,3)
			
			% If traintime == testtime perform standard cross validation
			if traintime == testtime
				
				% Retrieve the variables and target output of the samples
				% in each fold.
				pattern = time_evolution_counts(:,:,traintime);

				% If there exist neurons in any time point that have only
				% zeros across all trials, throw them out.
% 			     to_keep = ~all(pattern == 0, 2);
% 			     pattern = pattern(to_keep,:);

				% If there exist neurons in any time point that have small
                % spike count (across all trials) after cue presentation,
				% throw them out.
% 			    curr_time = 1 + time_step*(i-1);
% 			     if curr_time >= zero
				to_keep = ~all(pattern < 10, 2);
				pattern = pattern(to_keep,:);
% 			     end

				% Select the number of features to be fed to the classifier
				% and save this number for future reference.
				nFeatures = size(pattern,1);
				num_feat(traintime,traintime) = nFeatures;

				% Inner loop: CV for parameter selection and accuracy
				% estimation - use "cross_val".
				[mean_accuracy] = cross_val(pattern, labels, num_folds, ...
                                            kernel_type);

				mean_accuracies(traintime,traintime) = mean_accuracy;
				
			% If traintime != testtime perform cross-temporal cross
            % validation.
			elseif traintime ~= testtime
				
				% Select training data.
				train_data = time_evolution_counts(:,:,traintime);
				
				% Select test data.
				test_data = time_evolution_counts(:,:,testtime);
				
				% If there exist neurons in any time point that have small
				% spike count (across all trials) after cue presentation,
                % throw them out.
				to_keep_train = ~all(train_data < 10, 2);
				to_keep_test = ~all(test_data < 10, 2);
				to_keep_both = to_keep_train | to_keep_test;
				% Then for both train and test sets keep the same indexes.
				train_data = train_data(to_keep_both,:);
				test_data = test_data(to_keep_both,:);

				% Select the number of features to be fed to the classifier
 				% and save this number for future reference.
% 				 nFeatures = size(train_data,1);
% 				 num_feat(traintime,traintime) = nFeatures;
% 				
% 				 fprintf('train, test %d %d \n', [traintime,testtime]);
				
				% Inner loop: CV for parameter selection and accuracy
				% estimation - use "cross_val"
				[mean_accuracy] = cross_val(train_data, labels, num_folds, ...
                                                kernel_type, test_data);
				mean_accuracies(traintime,testtime) = mean_accuracy;
				
			end
		end
	end
end