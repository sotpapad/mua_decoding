# MUA_decoding

Pipeline for decoding attributes of particular experimental tasks, based on multi-unit activity (MUA) recorded from certain brain areas during task execution (under the assumption that these areas are responsible for encoding these attributes).

This pipeline represents part of my work during the training I received in the [Laboratory of Visual Cognition (Gregoriou Lab)](http://physiology.med.uoc.gr/?n=LaboratoryOfVisualCognition.Home).

It is mostly inspired by the following research articles:
 - [Astrand E, Ibos G, Duhamel JR, Hamed S Ben (2015) Differential dynamics of spatial attention,
position, and color coding within the parietofrontal network. J Neurosci 35:3174–3189.](https://pubmed.ncbi.nlm.nih.gov/25698752/)
 - [Cavanagh SE, Towers JP, Wallis JD, Hunt LT, Kennerley SW (2018) Reconciling persistent and
dynamic hypotheses of working memory coding in prefrontal cortex. Nat Commun 9:1–16.](https://pubmed.ncbi.nlm.nih.gov/30158519/)
 - [Gregoriou GG, Rossi AF, Ungerleider LG, Desimone R (2014) Lesions of prefrontal cortex reduce
attentional modulation of neuronal responses and synchrony in V4. Nat Neurosci 17:1003–1011.](https://pubmed.ncbi.nlm.nih.gov/24929661/)
 - [Panzeri S, Senatore R, Montemurro MA, Petersen RS (2007) Correcting for the sampling bias problem
in spike train information measures. J Neurophysiol 98:1064–1072.](https://pubmed.ncbi.nlm.nih.gov/17615128/)
 - [Spaak E, Watanabe K, Funahashi S, Stokes MG (2017) Stable and dynamic coding for working
memory in primate prefrontal cortex. J Neurosci 37:6503–6516.](https://pubmed.ncbi.nlm.nih.gov/28559375/)
 - [Tremblay S, Pieper F, Sachs A, Martinez-Trujillo J (2015) Attentional Filtering of Visual Information
by Neuronal Ensembles in the Primate Lateral Prefrontal Cortex. Neuron 85:202–215.](https://pubmed.ncbi.nlm.nih.gov/25500502/)

