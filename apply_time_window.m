% Function that parses all the data selceted by "data_for_clsf" and
% returns a matrix whose pages correspond to the time points selected by
% moving a sliding window.

% Inputs:
%       patterns: cell array of condition-specific data containing only the
%                 samples (trials) with min_trials, according to "data_for_clsf"
%       time_window: duration of sliding time window (in msec) that is
%                    used for training a classifier - it controls the time
%                    evolution of the decoding analysis
%       time_step: amount of time points that the windows slides (in msec)
%       init_time: initial time point for time window application
%       num_windows: number of windows that feet into the whole duration of
%                    a trial

% Outputs:
%       time_evolution_counts: 3D matrix contianing the spike count of each
%                              window in time_evolution_data

function [time_evolution_counts] = apply_time_window(patterns, time_window, ...
                                        time_step, init_time, num_windows)

    % Get the "patterns" dimensions
    num_features = size(patterns,1);
    num_samples = size(patterns,2);

   % Create the cell array of the time evolution
	time_evolution_counts = zeros(num_features,num_samples,num_windows);
	new_num_features = num_features;
	
    % Fill the time_evolution_data cell array:
    % starting from the time point "init_time", slide the window of
    % duration "time_window" over each cell of the patterns; when one loop
    % is completed, store in the current page of "time_evolution_data",
    % advance page and repeat
    for w=1:num_windows
        curr_time = init_time + time_step*(w-1);
        for row=1:new_num_features
            for col=1:num_samples
                eval = patterns{row,col};
                time_evolution_data = eval(curr_time:curr_time+(time_window-1));
                % spikes count in time window
                time_evolution_counts(row,col,w) = sum(time_evolution_data);
            end
        end
    end
	
end