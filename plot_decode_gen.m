% Function that quantifies how two different conditions differ in terms of
% decoding generalization.

% Inputs:
%        p_values_chance_1: p-values obtained comparing each time point in
%                           the 2D cross-temporal matrix against the null
%                           distribution for condition 1
%        p_values_chance_2: same for condition 2
%        p_values_diag_1: p-values obtained comparing each time point in
%                         the 2D cross-temporal matrix against the values
%                         on the diagonal for condition 1
%        p_values_diag_2: same for condition 2
%		 area: area where the data come from
%		 task: task
%		 p_cutoffs: cut-off values for statistical significance depiction

% Outputs:
%         plot that quantifies how two different conditions differ in terms
%         of decoding generalization, given two different methods of
%         statistical significance estimation

function [] = plot_decode_gen(p_values_chance_1, p_values_chance_2,...
                                p_values_diag_1, p_values_diag_2,...
                                area, task, p_cutoffs)
    
    % Initialization of arrays.
    count_chance_1 = nan(1,size(p_values_chance_1,1));
    count_chance_2 = nan(1,size(count_chance_1,2));
    count_diag_1 = zeros(1,size(count_chance_1,2));
    count_diag_2 = zeros(1,size(count_chance_1,2));
    
    px2_chance = nan(1,size(p_values_chance_1,1));
    px2_diag = nan(1,size(p_values_chance_1,1));
    sign_t_chance = nan(1,size(p_values_chance_1,1));
    sign_t_diag = nan(1,size(p_values_chance_1,1));
                     
	% Extraction of diagonal values for chance.
	diag_chance_1 = nan(1,size(p_values_chance_1,1));
	diag_chance_2 = nan(1,size(p_values_chance_2,1));
	for tr=1:size(p_values_chance_1,2)
		diag_chance_1(1,tr) = p_values_chance_1(tr,tr);
		diag_chance_2(1,tr) = p_values_chance_2(tr,tr);
	end
	
    % Look into every training time point:
    for tr=1:size(p_values_chance_1,2)
        % for the "chance" p-values look for those values that are <= 0.001
        count_chance_1(1,tr) = sum( p_values_chance_1(:,tr) <= 0.001 );
        count_chance_2(1,tr) = sum( p_values_chance_2(:,tr) <= 0.001 );
    end
    
	% For the "diagonal" p-values look for those values that are 0
	% (in this case 1 signifies time points that do not differ from the
	% diagonal, hence the code generalizes), but are also different
	% from chance.
	
	% For every diagonal condition check if the diagonal contains
	% non-significant values compared to chance.
	if ~all(diag_chance_1 <= 0.001)
		for tr=1:size(p_values_chance_1,2)
			for ts=1:size(p_values_chance_1,1)
				if ~(p_values_chance_1(tr,tr) <= 0.001) || ~(p_values_chance_1(ts,tr) <= 0.001)
					% if chance estimation dictates non-significance, then
					% also the diagonal estimation becomes non-significant
					% (the value differs from the diagonal value)
					p_values_diag_1(ts,tr) = 0.0;
				end
			end
			% number of statistically non-significant different time points
			count_diag_1(1,tr) = sum( p_values_diag_1(:,tr) == 1 );
		end
	else
		for tr=1:size(p_values_chance_1,2)
			% number of statistically non-significant different time points
			count_diag_1(1,tr) = sum( p_values_diag_1(:,tr) == 1 );
		end
	end
		
	if ~all(diag_chance_2 <= 0.001)
		for tr=1:size(p_values_chance_2,2)
			for ts=1:size(p_values_chance_2,1)
				if ~(p_values_chance_2(tr,tr) <= 0.001) || ~(p_values_chance_2(ts,tr) <= 0.001)
					% if chance estimation dictates non-significance, then
					% also the diagonal estimation becomes non-significant
					% (the value differs from the diagonal value)
					p_values_diag_2(ts,tr) = 0.0;
				end
			end
			% number of statistically non-significant different time points
			count_diag_2(1,tr) = sum( p_values_diag_2(:,tr) == 1 );
		end
	else
		for tr=1:size(p_values_chance_2,2)
			% number of statistically non-significant different time points
			count_diag_2(1,tr) = sum( p_values_diag_2(:,tr) == 1 );
		end
	end
    
    % Perform X^2 test among "chance" and "diagonal" counts in pairs.
    total_tp = size(p_values_chance_1,2);
    % X-axis creation.
    total_time = 1:total_tp;
    % For every training time point:
    for tr=1:total_tp
        % find number of significant test points
        n1_ch = count_chance_1(1,tr);
        n2_ch = count_chance_2(1,tr);
        n1_d = count_diag_1(1,tr);
        n2_d = count_diag_2(1,tr);
        % create first array for cross-tabulation
        x = [repmat('a',1,total_tp), repmat('b',1,total_tp)];
        % create second array for cross-tabulation
        x_ch = [ones(1,n1_ch), repmat(2,1,total_tp-n1_ch),...
                    ones(1,n2_ch), repmat(2,1,total_tp-n2_ch)];
        x_d = [ones(1,n1_d), repmat(2,1,total_tp-n1_d),...
                    ones(1,n2_d), repmat(2,1,total_tp-n2_d)];
        % perform cross-tabulation
        [~,~,px2_ch] = crosstab(x.',x_ch.');
        [~,~,px2_d] = crosstab(x.',x_d.');
        % keep the result
        px2_chance(1,tr) = px2_ch;
        px2_diag(1,tr) = px2_d;
    end
    
    % Evaluate the statistical significance.
    for tr=1:total_tp
        sign_t_chance = st_level(sign_t_chance, total_time, tr, px2_chance, p_cutoffs);
        sign_t_diag = st_level(sign_t_diag, total_time, tr, px2_diag, p_cutoffs);
    end
	
    % 2D plots
	for i=1:2
		figure;
		if i==1
			plot(total_time,count_chance_1,total_time,count_chance_2,'Linewidth',2);
		elseif i==2
			plot(total_time,count_diag_1,total_time,count_diag_2,'Linewidth',2);
		end
		
		% Illustrate statistical significance.
		sign_data = cell(1,1);
		if i==1
			sign_data{1,1} = sign_t_chance;
			% control height of plot
			height = max( [max(count_chance_1), max(count_chance_2)] ) + 5;
		elseif i==2
			sign_data{1,1} = sign_t_diag;
			% control height of plot
			height = max( [max(count_diag_1), max(count_diag_2)] ) + 5;
		end
		no_lines = 'one';
		plot_sign_line_ct(total_time, no_lines, sign_data, height);
		
		% Plot the onset of another experimental period of interest..
		delay = 90;     % example time point
		hold on;
		plot([delay delay], [0 height-2], '--k', 'LineWidth', 1.5);

		% Labels.
		xlabel('Training time from cue onset (s)');
		if i==1
			ylabel({'Number of statistically significant test time points,';...
						'when compared to chance'});
		elseif i==2
			ylabel({'Number of statistically significant test time points';...
						'with above-chance accuracy, when compared to diagonal'});
		end

		% Ticklabels.
		% the data are from 101 (0 ms) to 1301 (1200 ms) - step is 10 ms
		ax = gca;
		set(ax, 'xtick', [1:20:size(p_values_chance_1,1)]);
		set(ax, 'xticklabels', [0.0+0.05:0.2:1.1+0.05]);
		if height >= 30
			set(ax, 'ytick', [0:20:height]);
			set(ax, 'yticklabels', {'0', '20/111', '40/111', '60/111', '80/111',...
									'100/111', '120/111'});
		else
			set(ax, 'ytick', [0:5:height]);
			set(ax, 'yticklabels', {'0', '5/111', '10/111', '15/111', '20/111',...
									'25/111'});
		end

		% Axis limits.
		set(ax, 'xlim', [1 size(p_values_chance_1,1)]);
		set(ax, 'ylim', [1 (height + 7)]);

		% Title.
		if i==1
			title( strcat( strcat( strcat(area,':',' '), task), ' - chance') );
		elseif i==2
			title( strcat( strcat( strcat(area,':',' '), task), ' - diagonal') );
		end

		% Legend.
		legend_strs = {'condition_1', 'condition_2'};
		legend(legend_strs,'Location','best');
		legend('boxoff');
	end
end