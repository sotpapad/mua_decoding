% Function that peroforms Wilcoxon tests, for comparing data from different
% conditions or areas.
% The procedure is performed for each time step in the data.

% Inputs:
%       acc1: accuracies from data - 1
%       acc2: accuracies from data - 2
%       analysis_type: type of permutations to perform ("one" or "two" - sided)

% Outputs:
%       p_values: one- or two-sided p-value for observed data,l determined
%                 by "type" input

function [p_values] = wilcoxon(acc1,acc2,analysis_type)

    % Analysis of observed distribution vs chance.
    if strcmpi(analysis_type,'one')
        p_values = nan(1,length(acc1));
        for tp=1:length(acc1)
            p_values(1,tp) = signrank(acc1(:,tp), acc2(:,tp));
        end
    
    % Analysis of observed distribution 1 vs observed distribution 2.
    elseif strcmpi(analysis_type,'two')
        p_values = nan(2,size(acc1,2));
        for tp=1:size(acc1,2)
            p_values(1,tp) = signrank(acc1(:,tp), acc2(:,tp));
            p_values(2,tp) = signrank(acc2(:,tp), acc1(:,tp));
        end
    end

end