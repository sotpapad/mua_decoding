% Function that performs permutation tests, for comparing cross-temporal
% data. The procedure is performed for each time step in the data.

% Inputs:
%       acc: "real" accuracies from data
%       nacc: null accuracies from data
%       analysis_type: type of permutations to perform ("chance" or "diagonal")

% Outputs:
%       p_values: one-sided p-values for observed data

function [p_values] = perm_ct_chance_diag(acc, nacc, analysis_type)

    % Comparison of "real" and chance accuracies.
    if strcmpi(analysis_type,'chance')

        % Initialize p-values array for each time step.
        p_values = nan(size(acc,1),size(acc,2));

        % Mean of "real" accuracy.
        macc = mean(acc,3);

        % Number of shuffles.
        nsh = size(nacc,3);
	
		% Calculation of p-values.
		for tr=1:size(acc,2)
			for ts=1:size(acc,1)
				p_values(ts,tr) = sum( macc(ts,tr)<nacc(ts,tr,:)) / nsh;
			end
		end

	% Comparison of "real" accuracies with the values of the diagonal
	% (tr==ts).
    % In this context the null accuracy is in reality the "real" accuracy
    % of the diagonal.
	elseif strcmpi(analysis_type,'diagonal')
        
        % Number of permutations.
        nperm = 100;
        
        % Initialize p-values array for each time step.
        p_values =  nan(size(acc,1),size(acc,2),2);
        
        % Initialize matrices of observed differences of any time point
        % with the two corresponding time points on the diagonal.
        obs_diff_tr = nan(size(acc,1),size(acc,2));
        obs_diff_ts = nan(size(acc,1),size(acc,2));
        
        % Initialize the null distribution matrix.
        null_dist_tr = nan(size(acc,1),size(acc,2),nperm);
        null_dist_ts = nan(size(acc,1),size(acc,2),nperm);
        
        % Perform permutations.
        for p=1:nperm
            % Select random indices to permute at each time step.
            for tr=1:size(acc,2)
                for ts=1:size(acc,1)
                    % Concatenate the two data matrices (2 times, so as to
                    % be able to compare any time point with 2 points along
                    % the diagonal).
                    all_acc_tr = [squeeze(acc(ts,tr,:)); squeeze(nacc(tr,tr,:))];
                    all_acc_ts = [squeeze(acc(ts,tr,:)); squeeze(nacc(ts,ts,:))];
                    % for every time point except those on the diagonal:
                    if tr ~= ts
                        indices = randperm(size(all_acc_tr,1));
                        this_point_this_perm_tr = all_acc_tr(indices);
                        this_point_this_perm_ts = all_acc_ts(indices);
                        % split the data
                        acc1_rand_tr = this_point_this_perm_tr(1:size(acc,3));
                        acc2_rand_tr = this_point_this_perm_tr((size(acc,3)+1):end);
                        acc1_rand_ts = this_point_this_perm_ts(1:size(acc,3));
                        acc2_rand_ts = this_point_this_perm_ts((size(acc,3)+1):end);
                        % calculate null distribution
                        null_dist_tr(ts,tr,p) = mean(acc1_rand_tr) - mean(acc2_rand_tr);
                        null_dist_ts(ts,tr,p) = mean(acc1_rand_ts) - mean(acc2_rand_ts);
                    % the points on the diagonal are considered significant
                    else
                        null_dist_tr(ts,tr,p) = 0.0;
                        null_dist_ts(ts,tr,p) = 0.0;
                    end
                end
            end
        end
        
        % Sort the null distributions at each time step.
        null_dist_tr = sort(null_dist_tr,3);
        null_dist_ts = sort(null_dist_ts,3);
        
        % When all estimations are available, a time point is statistically
        % significant if both estimated p-values are.
        % For this, I set a (not strict) cut-off of a=0.001
        a = 0.001;
        
        % Then if a time point is significant, I use the mean p-value as
        % its actual p-value.
        composite_p_values = nan(size(p_values,1),size(p_values,2));
        
        % Calculate p-values for each time point.
        for tr=1:size(acc,2)
            for ts=1:size(acc,1)
                % for every time point except those on the diagonal:
                if tr ~= ts
                    % calculate the observed difference per time step
                    % (the hypothesis is that nacc >= acc)
                    obs_diff_tr(ts,tr) = [mean(nacc(ts,tr,:)) - mean(acc(tr,tr,:))];
                    obs_diff_ts(ts,tr) = [mean(nacc(ts,tr,:)) - mean(acc(ts,ts,:))];

                    if obs_diff_tr(ts,tr) < 0.0
                        p_values(ts,tr,1) = sum( null_dist_tr(ts,tr,:) < obs_diff_tr(ts,tr) ) / nperm;
                    elseif obs_diff_tr(ts,tr) >= 0.0
                        p_values(ts,tr,1) = sum( null_dist_tr(ts,tr,:) > obs_diff_tr(ts,tr) ) / nperm;
                    end
                    
                    if obs_diff_ts(ts,tr) < 0.0
                        p_values(ts,tr,2) = sum( null_dist_ts(ts,tr,:) < obs_diff_ts(ts,tr) ) / nperm;
                    elseif obs_diff_ts(tr,ts) >= 0.0
                        p_values(ts,tr,2) = sum( null_dist_ts(ts,tr,:) > obs_diff_ts(ts,tr) ) / nperm;
                    end
                    
                    % if both p-values are statistically significant
                    % compute their mean
                    if p_values(ts,tr,1) <= a && p_values(ts,tr,2) <= a
                        composite_p_values(ts,tr) = mean(p_values(ts,tr,:));
                    else
                        % if they are not, (arbitrarily) place a one
                        composite_p_values(ts,tr) = 1.0;
                    end
                    
                % for the time points on the diagonal place a one as well,
                % as they do not differ from themselves!
                else
                    composite_p_values(ts,tr) = 1.0;
                end
            end
        end
        
        % Return the mean p-values.
        p_values = composite_p_values;
    end
end