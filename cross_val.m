% Function that performs the inner loop of the Nested Cross Validation
% using the LIBSVM library for training and testing a model.

% Inputs:
%       pattern: current spike time counts (specific time window)
%       labels: vector of labels of condition variation for every trial
%       num_folds: number of folds of the nested CV
%       kernel_type: type of kernel used for the SVM classifier
%                   (linear / rbf)
%       test_pattern: current test spike time counts, passed in case of
%                     cross-temporal decoding only

%Outputs:
%       mean_accuracy: mean accuracy of model

function [mean_accuracy] = cross_val(pattern, labels, num_folds, ...
                                        kernel_type, test_pattern)

    % Try up to 'num_folds' number of folds
    for i=1:num_folds
        % Data splitting according to 'num_folds' to training and test sets.
        fold = floor(size(pattern, 2)/num_folds);
        test_indices   = 1 + (i-1)*fold : i * fold;
        train_indices  = [1:(i-1)*fold, i * fold + 1:size(pattern, 2)];

        train_patterns = pattern(:, train_indices);
        train_labels  = labels (:, train_indices);
        if ~exist('test_pattern', 'var')
            test_patterns  = pattern(:, test_indices);
        else
            test_patterns  = test_pattern(:, test_indices);
        end
        test_labels   = labels (:, test_indices);

        % The matrices should be transposed.
        train_patterns = train_patterns';
        test_patterns  = test_patterns';
        train_labels  = train_labels';
        test_labels   = test_labels';
        
        % Perform scaling of spike count train data and use this scale to
        % scale test data (be sure to use the same original range).
        [train_data_sc, train_scale] = scale_data(train_patterns, [0 1]);
        [test_data_sc, ~] = scale_data(test_patterns, [0 1], train_scale);
        
        % Evaluate kernel type and choose best model on this kernel.
        switch kernel_type
            % Perform parameter selection.
            case 'rbf'
                [bestc] = param_select(train_data_sc, train_labels, ...
                                                    kernel_type, num_folds);
                cmd = ['-t 2 -c ',  num2str(bestc), ' -g ',num2str(bestg), '-q'];
            case 'linear'
                [bestc] = param_select(train_data_sc, train_labels, ...
                                            kernel_type, num_folds);
                cmd = ['-t 0 -c ',  num2str(bestc)];
        end
        [~,model] = evalc('libsvmtrain(train_targets, train_data_sc, cmd);');
        
        % Evaluate the selected model.
        [~, accuracy(i,:), ~] = svmpredict(test_labels, test_data_sc, model, '-q');
                                                
    end
    
    % Keep mean accuracy.
    mean_accuracy = mean(accuracy(:, 1));
    
end