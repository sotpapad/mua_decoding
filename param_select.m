% Functions that selects the best parameters for the cross validation step.

% Inputs:
%       train_data: scaled training data
%       labels: vector of labels of condition variation for every trial
%       num_folds: number of folds of the nested CV
%       kernel_type: type of kernel used for the SVM classifier
%                   (linear / rbf)

% Outputs:
%       best_cost: best cost parameter for SVM

function [best_cost] = param_select(train_data, labels, num_folds, ...
                                        kernel_type)
    
    % Best score set to zero.
    bestrate = 0;
                                    
    % Check 'kernel_type'.
    switch kernel_type
        
        % 'Rbf' kernel has an extra gamma parameter to tune.
        case 'rbf'
            for log2c = -5:5
                for log2g = -10:0
                    % -t kernel_type, 0 linear, 2 RDF
                    % -v number of folds for cross validation
                    % -c cost parameter
                    % -g gamma parameter (not required for linear)
                    cmd = ['-q -t 2 -v ', num2str(num_folds), ' -c ', ...
                            num2str(2^log2c), ' -g ', num2str(2^log2g)];

                    [~,cv] = evalc('libsvmtrain(labels, train_data, cmd);');
                    
                    if (cv >= bestrate)
                        bestrate = cv;
                        best_cost = 2^log2c;
                    end

                end
            end
        
        % 'Linear' kernel.
        case 'linear'
            for log2c = -5:5
                % -t kernel_type, 0 linear, 2 RDF
                % -v number of folds for cross validation
                % -c cost parameter
                cmd = ['-t 0 -v ', num2str(num_folds), ' -c ', num2str(2^log2c)];

                [~,cv] = evalc('libsvmtrain(labesl, train_data, cmd);');
                
                if (cv >= bestrate)
                    bestrate = cv; best_cost = 2^log2c;
                end
            end

    end
    
end