% Function that allows for selection of area of interest for analysis
% The function is case insensitive

% Inputs:
%       fullpath: all recordings from a particular experimental condition
%       area: area of recordings

% Outputs:
%       sel_data: recordings of the experimental condition corresponding
%                 only to the area of interest
%       last_col: number indicating in which column the positions of
%                      the recordings are described

function [sel_data, last_col] = select_area(fullpath, area)

    % Load the data.
    load(fullpath);
    
    % Keep the recordings' data, in case any other structure is present in
    % the data.
    % In general the data should have a "datsq" variable; if not, raise an
    % error, inform about the missing variable, and abort.
    try
        rec_data = datsq;
    catch ME
        switch ME.identifier
            case 'MATLAB:UndefinedFunction'
                msg = ['No variable "datsq" was found in the loaded file. '...
                        'Are you loading an "spkraw.mat" file?'];
                fprintf(msg);
        end
    end

    % Determine the column with probes names: remember that only the last
    % column of the data does not correspond to recordings.
    last_col = size(rec_data,2);
	
    if strcmpi(area,'brain_area_1')
        % Keep only blocks of recordings from 'brain_area_1' channels.
        ba1_channels = {'SPK_1_1', 'SPK_2_1', 'SPK_3_1', 'SPK_4_1'}; % or more
        ba1_indexes = zeros(size(rec_data,1),last_col-1);
        for ch=1:length(ba1_channels)
            ba1_indexes(:,ch) = strcmp(rec_data(:,last_col),ba1_channels{ch});
        end
        % Combine the indexes of the 'brain_area_1' recordings.
        % (this corresponds to per-row summation of all the columns of the
        % logical indexing performed above)
        ba1_indexes = sum(ba1_indexes,2);
        ba1_indexes = logical(ba1_indexes);
        sel_data = rec_data(ba1_indexes,1:last_col-1);
    
    elseif strcmpi(area,'brain_area_2')
        % Keep only blocks of recordings from 'brain_area_2' channels.
        ba2_channels = {'SPK_9_1', 'SPK_10_1', 'SPK_11_1', 'SPK_12_1'};
        ba2_indexes = zeros(size(rec_data,1),last_col-1);
        for ch=1:length(ba2_channels)
            ba2_indexes(:,ch) = strcmp(rec_data(:,last_col),ba2_channels{ch});
        end
        % Combine the indexes of the 'brain_area_2' recordings.
        % (this corresponds to per-row summation of all the columns of the
        % logical indexing performed above)
        ba2_indexes = sum(ba2_indexes,2);
        ba2_indexes = logical(ba2_indexes);
        sel_data = rec_data(ba2_indexes,1:last_col-1);    
    end

    % Make sure that no empty cells are present in the sel_data.
    to_keep = ones(size(sel_data,1),1);
    for r=1:size(sel_data,1)
        % Check only the first column - could there be trials only on
        % some???
        if isempty(sel_data{r,1})
            to_keep(r,1) = 0;
        end
    end
    to_keep = logical(to_keep);
    sel_data = sel_data(to_keep,:);
            
end