% Function that plots the accuracies of the classifier trained in each time
% step for spike count

% Inputs:
%       mean_accuracies: mean accuracies calculated by "McrossValidation"
%       sem_accuracies: sem of accuracies calculated by "McrossValidation"
%       p_values: p-value of observed data (compared to permutation disrtibution)
%       p_cutoff: cutoff for p-value statistical significance
%       analysis_type: string that specifies whether one or two conditions
%                      are plotted
%       area: area where the data come from
%       center: centering of data (e.e. cue presentation)
%       task: task where the data come from
%       time_window: duration of sliding time window (in msec) that is
%                    used for training a classifier - it controls the time
%                    evolution of the decoding analysis in "apply_time_window"
%       time_step: amount of time points that the windows slides (in msec)
%       init_time: initial time point for time window application
%       end_time: final time for time window application
%       min_trials: number of minimum trials that a neuron must have in
%                   order for us to have a good samples-to-features ratio
%       num_var: number of variations used in the analysis depicted
%       num_blocks: number of neurons used in a given decoding analysis

% Outputs:
%       accuracy plot: plot of time vs accuracy

function [] = plot_accuracy(mean_accuracies, sem_accuracies, p_values,...
                            p_cutoff, analysis_type, area, center, task,...
                            time_window, time_step, init_time, end_time,...
                            min_trials, num_var, num_blocks)

    % Creation of appropriate title string.
    t_str = [area, ', time window: ', num2str(time_window),...
                    'ms, time step: ', num2str(time_step), 'ms'];
    
    % Find the correct x-axis.
    % Remember the assumption is 1 time point per millisecond; if it does
    % not hold, change accordingly.
    time = 1:time_step:time_step*length(mean_accuracies);
    half_window = time_window/(2*1000);
    
    % Create the plot.
    figure;
    
    % Get the default color order of matlab.
    order = get(gca,'ColorOrder');
    
    % Time series of mean accuracy.
    plot(time,mean_accuracies(1,:));

    % Time series of sem accuracy.
    hold on;
    patch1 = fill([time fliplr(time)], [mean_accuracies(1,:),...
                    fliplr(mean_accuracies(1,:) + sem_accuracies(1,:))], order(1));
    patch2 = fill([time fliplr(time)], [mean_accuracies(1,:),...
                    fliplr(mean_accuracies(1,:) - sem_accuracies(1,:))], order(1));
    set(patch1, 'edgecolor', 'none');
    set(patch1, 'FaceAlpha', 0.25);
    set(patch2, 'edgecolor', 'none');
    set(patch2, 'FaceAlpha', 0.25);
    set(get(get(patch1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    set(get(get(patch2(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    if strcmpi(analysis_type,'two')
        % time series of mean accuracy
        hold on;
        plot(time,mean_accuracies(2,:));
        % time series of sem accuracy
        hold on;
        patch3 = fill([time fliplr(time)], [mean_accuracies(2,:),...
                        fliplr(mean_accuracies(2,:) + sem_accuracies(2,:))], order(2));
        patch4 = fill([time fliplr(time)], [mean_accuracies(2,:),...
                        fliplr(mean_accuracies(2,:) - sem_accuracies(2,:))], order(2));
        set(patch3, 'edgecolor', 'none');
        set(patch3, 'FaceAlpha', 0.25);
        set(patch4, 'edgecolor', 'none');
        set(patch4, 'FaceAlpha', 0.25);
        set(get(get(patch3(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        set(get(get(patch4(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    xlim([time(1) time(end)]);
    ylim([0 110]);
		
    % Axis ticks and labels (use "s" as unit of x axis, instead of "ms").
    ax = gca;
    ax.YTick = [20:20:100];
    % Example x ticks.
    % Example experimental condition.
    if strcmpi(center,'cue_presentation')
        % The time set in main.
        if init_time == 101
            set(ax, 'xtick', [1:150:1301]);
            set(ax, 'xticklabels', [-0.2+half_window:0.2-half_window:1.101+half_window]);
            cue_pres = 201-half_window*1000;
        elseif init_time == 201     % any other time point
            % TBD accordingly
        end
    % Different experimental condition.
    elseif strcmpi(center,'<TBD>')
        % and so on...
    end
    
    % Parse all the time steps; if 5 consecutive time steps have p_val <
    % p_cutoff, then this time step is statistically significant.
    sign_t = zeros(1,size(mean_accuracies,2));
    for t=1:length(time)
        [sign_t] = st_level(sign_t,time,t,p_values(1,:),p_cutoff);
    end
    if strcmpi(analysis_type,'two')
        % Calculate, also, significance of p-values for the second
        % condition.
        sign2_t = zeros(1,size(mean_accuracies,2));
        for t=1:length(time)
            [sign2_t] = st_level(sign2_t,time,t,p_values(2,:),p_cutoff);
        end
    end
    
    % Illustrate statistical significance.
    sign_data = cell(1,2);
    sign_data{1,1} = sign_t;
    if strcmpi(analysis_type,'two')
        sign_data{1,2} = sign2_t;
    end
    plot_sign_line(time, analysis_type, sign_data);

    % Plot a vertical line at the time point where the data are centered
    % on.
    hold on;
    plot([cue_pres cue_pres], [0 100], 'k', 'LineWidth', 0.8);
    
    % Set the theoretical (or expected) chance level.
    if num_var == 2
        chance = 50;
    elseif num_var == 3
        chance = 100/3;
    elseif num_var == 4
        chance = 25;
	
    % and so on...
	
    end

    
    % Plot a horizontal line at the theoretical chance level.
    hold on;
    plot([time(1) time(end)], [chance chance], '-.', 'Color', [0.5, 0.5, 0.5]);
	
    % Custom "legend" that specifies which variations are compared, and how
    % many trials are used.
	if num_var == 4
        variations = '<TBD: appropriate names>';
    elseif num_var == 3
        variations = '<TBD: appropriate names>';
        
    % and so on...
    
	end
    
    % Custom legend that specifies the variations used in the analysis
    % (as created above).
    vars_str = ['variations: ', variations];
	hor = 650;
	ver = 31;
	if num_var == 6
		hor = 1000;
		ver = 15;
	end
    text(end_time(end)-hor,ver,vars_str);
    
    % Custom legend that specifies the number of neurons included in the
    % analysis.
    if length(num_blocks) == 1
        neurons_str = ['n: ', num2str(num_blocks(1))];
        text(end_time(end)-hor,ver-4,neurons_str)
    elseif length(num_blocks) == 2
        if num_blocks(1) == num_blocks(2)
            neurons_str = ['n: ', num2str(num_blocks(1))];
            text(end_time(end)-hor,ver-4,neurons_str)
        else
            neurons_str = ['n: ', num2str(num_blocks(1)),' / ', num2str(num_blocks(2))];
            text(end_time(end)-hor,ver-4,neurons_str)
        end
    end
    
    % Custom legend that specifies the number of trials used in the
    % analysis.
    trials_str = ['trials: ', num2str(min_trials)];
    text(end_time(end)-hor,ver-8,trials_str);
    
    % Labels, title and legend.
	if strcmpi(center,'cue_presentation')
		xlabel('Time from cue onset (s)');
	elseif strcmpi(center,'<TBD>')
		% and so on...
	end
	ylabel('Accuarcy (%)');
    title(t_str);
    
    % Only in case of multiple areas or conditions.
    if strcmpi(task,'<TBD: experimental_task_1_condition_1_vs_2>')
        legend('condition_1','condition_2', [330 250 10.0 10.0]); % this tuple specifies location
        legend('boxoff');
    elseif strcmpi(task,'TBD: brain_area_1_condition_1_vs_brain_area_2_condition_1')
        legend('brain_area_1','brain_area_2', [330 250 10.0 10.0]);
        legend('boxoff');
    elseif strcmpi(task,'<TBD: any valid comparison>')
        % and so on...
    end

end