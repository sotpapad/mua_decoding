%% 0. Decoding Spike Counts main script

% Multi-unit activity (MUA) decoding. Basic assumptions for the data
% include:
%   - concurrent recordings from (at least) 2 brain areas with arrays of K
%     probes,
%   - recordings come from different days and are concatenated in order to
%     create pseudo-populations; each day some probes are inserted into
%     the brain and multiple blocks of trials of a task and condition(s)
%     are performed with some variations V,
%   - the recordings of all trials are kept for each probe of each day,
%   - recordings comprise N time points, N being equal for the same
%     experimental task,
%   - all data have been pre-processed, that is centered according to a
%     specific task event and recordings of law quality have been discarded

% Therefore the dataset is assumed to have the following form:

%   { probe_1_block_1_variation_1, ..., probe_1_block_1_variation_V, channel_name_1
%     probe_2_block_1_variation_1, ..., probe_K_block_1_variation_V, channel_name_2
%     ...
%     probe_K_block_B_variation_1, ..., probe_K_probe_B_variation_V, channel_name_K }

% with (cell) probe_k_probe_b_variation_v: (trials x N)

% As an example (later on) I have chosen to represent K=4, V=2-4.


% The LIBSVM library is used for performing the model training and testing.


% TBD signifies parts of the code that you should fill in.


% All functions can be called from sections of this script.

% Run Sections 1-4 together: create transfomred data from raw data.
% (Section 3 is optional, for parameter tunning - see below)

% Sections 5 and 6 constitute part of a for loop in Section 4:
%   - section 5 performs nested cross validation on standard decoding
%     analysis,
%   - section 6 performs nested cross validation on cross-temporal decoding
%     analysis.

% Run Section 7 together with 8, or 9 together with 10 independently from
% 1-5: permutation tests and plots for cross-temporal decoding.

%% 1. Directories defintions - RUN THIS SECTION IF YOU WANT TO RUN ANY SECTION UP TO 6 

% Absolute directories' paths are used for loading the data: the script
% will run in any working directory, and will save any results in that
% directory.

% You should modify this section in order to match the organization of your
% data. Here follows a sample code.

% Definition of the directory/directories where data are stored.
experimental_task_1_dir = '<TBD: absolute path>';
experimental_task_2_dir = '<TBD: absolute path>';

% Creation of different names depending on the experimental setup and task
% condition.
task_1_condition_1_to_n_filename_suffixes = {'<TBD: something.mat>'};
task_2_condition_1_to_n_filename_suffixes = {'<TBD: something.mat>'};

for n=1:length(task_1_condition_1_to_n_filename_suffixes)
    experimental_setup_1_fullpaths = strcat(experimental_task_1_dir,...
                                        task_1_condition_1_to_n_filename_suffixes{n});
end

for n=1:length(task_2_condition_1_to_n_filename_suffixes)
    experimental_setup_2_fullpaths = strcat(experimental_task_2_dir,...
                                        task_2_condition_1_to_n_filename_suffixes{n});
end

%% 2. Main variables defintions - RUN THIS SECTION IF YOU WANT TO RUN ANY SECTION UP TO 6 

% Selection of brain area of recordings.
area = 'brain_area_1';
%area = 'brain_area_2';

% Selection of minimum number of trials, sliding time window duration, as
% well as the time step.
min_trials = 25;    % check histograms of section 3 to find a suitable value
time_window = 100;
time_step = 10;

% Selection number of variations to be explored for each condition of the
% task.
% Variations reflect differences in the task condition, e.g. the appearence
% of a target of attention or movement to a different part of the space.
% If it is unclear which variations should be used (e.g. because a certain
% variation has is represented significantly with fewer trials), you should
% get a rough estimate using section 3.
num_var = 4;

% Selection of analysis type:
%   - either single area analysis (decoding of specific task condition
%     against chance levels),
%   - comparative analysis (decoding of specific task condition in one area
%     against the same condition in a second area, or possibly an alternate
%     condition from the first area)
analysis_type = 'single';
%analysis_type = 'comparative';

% If 'comparative' analysis has been selected, then we should use the same
% number of recordings probes for both areas - a logical choice would be to
% use: min(no_of_rec_in_area_1, no_of_rec_in_area_2).
% If "analysis_type == 'single'" it is only passed for consistency purposes.
subset = 100;

% Selection of standard decoding analysis vs cross-temporal decoding.
cross_temp = 0;     % standard decoding
%cross_temp = 1;	% cross-temporal decoding

% Having selected a certain task, it is now time to create some additional
% variables:
%   - filename, task, center: to be used for naming the results
%   - init_time: assuming the recordings comprise N time points, this
%           variable reflects the time point n where the analysis should
%           begin from (probably some ms before "zero")
%   - end_time: accordingly, this variable indicates the time point n where
%           the analysis should end
%   - zero: the time point n you consider as the beggining of the task (it
%           should probably coincide with the time point c you have
%           centered the data on, but it is not mandatory)

% The selection is accomplished with this flag.
flag = 11;

if flag == 11
    % Select here experimental setup: task_1, condition 1
    filename = task_1_condition_1_to_n_filename_suffixes{1};
    fullpath = experimental_setup_1_fullpaths{1};
    task = '<TBD: a name for the task>';
    % E.g. if N=1500, 1 time point per millisecond, centered on cue
    % presentation:
    center = 'cue';
    zero = 301;
    init_time = 101;    % look from 200ms before task onset
    end_time = 1301;    % look up to 1000ms after task onset
elseif flag == 12
    filename = task_1_condition_1_to_n_filename_suffixes{2};
    fullpath = experimental_setup_1_fullpaths{2};
    % and so on...
elseif flag == 21
    filename = task_2_condition_1_to_n_filename_suffixes{1};
    fullpath = experimental_setup_2_fullpaths{1};
    % and so on...
end

%% 3. Create histograms of trials per condition variation, in each task and area 

% The function "hist_trials_per_pos" takes as input the full path of the
% file containing the data (created in the previous section), as well as
% the area that is of interest.

% Call "hist_trials_per_pos".
hist_trials_per_pos(fullpath, area);

%% 4. Rearrange the data, so that they become a suitable input for the SVM 
% Then, perform the nested cross validation with the SVM classifiers.

% Before starting the loop, select blocks of trials from a specific area
% and with a minimum number of trials (sections 1,2).
fprintf('<strong>Initiating feature extraction for area:</strong> %s\n', area);
fprintf('<strong>Selected task:</strong> %s\n', task);
fprintf('<strong>Data centered on:</strong> %s\n', center);
fprintf('<strong>Number of trials used:</strong> %d\n', min_trials);
fprintf('<strong>Time step used:</strong> %d\n', time_step);

% Create the appropriate features for the classifier.
[features, labels] = extract_features(fullpath, area, num_var, min_trials,...
										analysis_type, subset);

% Keep the number of blocks in this analysis in a variable.
num_blocks = size(features,1);
fprintf('<strong>Number of neurons used:</strong> %d\n', num_blocks);

% Compute the total time duration.
total_time = end_time - init_time;

% Compute how many windows of duration time_window can fit into the
% total time.
num_windows = floor( (total_time - time_window) / time_step ) + 1;
fprintf('<strong>Number of time windows used:</strong> %d\n', num_windows);

% In order to obtain statistically significant results, we need to repeat
% the procedure.
if cross_temp == 0
	min_reps = 50;
	max_reps = 500;
elseif cross_temp == 1
	% Less repetitions when performing cross-temporal decoding due to
	% computational complexity; increase 'max_reps' for increased
	% statistical strength.
	min_reps = 50;
	max_reps = 50;
end

if strcmpi(analysis_type,'single')
    % when chance estimation is needed (single area-condition) max_reps is selected
    reps = max_reps;
elseif strcmpi(analysis_type,'comparative')
    % when comparison of two areas / conditions is needed select min_reps
    reps = min_reps;
end

if cross_temp == 0
	% Initialize arrays that will store the accuracies and the number of
	% fetaures used in the cross validation step.
	all_accuracies = zeros(min_reps, num_windows);
	all_features = zeros(min_reps, num_windows);

	% In case we want to estimate the decoding accuracy of a single
	% condition, except for the real accuracy estimation we, also, need a
	% distribution of null accuracies that will determine the chance level.
	if strcmpi(analysis_type,'single')
		null_accuracies = zeros(max_reps, num_windows);
	end
elseif cross_temp == 1
	% Initialize arrays that will store the accuracies and the number of
	% fetaures used in the cross validation step.
	all_accuracies = zeros(num_windows, num_windows, min_reps);
%	all_features = zeros(num_windows, num_windows, min_reps);

	% In case we want to estimate the decoding accuracy of a single
	% condition, except for the real accuracy estimation we, also, need a
	% distribution of null accuracies that will determine the chance level.
	if strcmpi(analysis_type,'single')
		null_accuracies = zeros(num_windows, num_windows, max_reps);
	end
end

% specify that spikes are used
signal_type = 'spikes';

% Define the number of folds for outer and inner cv steps.
num_folds = 5;

% Define the classifier's kernel type.
kernel_type = 'linear';
%kernel_type = 'rbf';

% Perform standard decoding analysis.
if cross_temp == 0
	% Start the accuracy estimation.
	parfor r=1:reps
		% The function "data_for_clsf" takes as input the full path of the
		% data of a particular experimental condition and outputs the data that
		% contain at least a certain number of trials from a certain area, in a 3D
		% matrix, where pages denote the time evolution; it, also, outputs the
		% corresponding labels vector.
		[time_evolution_counts] = data_for_clsf(features, min_trials, time_window,...
							time_step, init_time, num_windows);

		% Print progress.
		fprintf('Finished with repetition of data selection no: %d / %d\n', [r,reps]);    

		%% 5. Perform Nested 5-fold Cross validation while training SVM classifiers

		% Call nested cross validation for accuracy estimation.
		if r <= min_reps
			[all_accuracies(r,:), all_features(r,:)] = nested_CV(time_evolution_counts,...
											labels, time_step, zero, num_folds,...
											kernel_type);

			fprintf('Finished with nested CV accuracy estimation: %d / %d\n', [r,reps]);
		end

		% In case that the accuracy of a single condition is needed, then
		% we permute the labels and perform yet another nested cv, this
		% time up to "max_reps" times.
		if strcmpi(analysis_type,'single')
			scrambled = randperm(length(labels));
			scrambled_labels = labels(scrambled);

			[null_accuracies(r,:),~] = nested_CV(time_evolution_counts, scrambled_labels,...
										time_step, zero, num_folds, kernel_type);

			fprintf('Finished nested CV chance accuracy estimation: %d / %d\n', [r,reps]);
		end
	end

% Perform cross-temporal decoding analysis.
elseif cross_temp == 1
	parfor r=1:reps
	% The function "data_for_clsf" takes as input the full path of the
	% data of a particular experimental condition and outputs the data that
	% contain at least a certain number of trials from a certain area, in a 3D
	% matrix, where pages denote the time evolution; it, also, outputs the
	% corresponding labels vector.
	[time_evolution_counts] = data_for_clsf(features, min_trials, time_window,...
								time_step, init_time, num_windows);

	% Print progress.
	fprintf('Finished with repetition of data selection no: %d / %d\n', [r,reps]);
	
	%% 6. Perfrom Nested 5-fold Cross Validation with SVM on cross-temporal code
		
	% Call nested cross validation for accuracy estimation.
	if r <= min_reps
		[all_accuracies(:,:,r), ~] = nested_CV_cross_temp(time_evolution_counts,...
			labels,time_step, zero, num_folds, kernel_type);
		
		fprintf('Finished with cross-temporal nested CV accuracy estimation: %d / %d\n', [r,reps]);
	end

	% Call nested cross validation for chance estimation.
	scrambled = randperm(length(labels));
	scrambled_labels = labels(scrambled);
	[null_accuracies(:,:,r), ~] = nested_CV_cross_temp(time_evolution_counts,...
											 scrambled_labels, time_step, zero,...
											 num_folds, kernel_type);

	fprintf('Finished with cross-temporal nested CV chance accuracy estimation: %d / %d\n', [r,reps]);
	end
end
fprintf('<strong>FINISHED ALL</strong>\n');

% Custom string generation to differentiate data for 'single' condition or
% 'comparative' analysis.
if strcmpi(analysis_type,'single')
    astr = '_sngl';
elseif strcmpi(analysis_type,'comparative')
    astr = '_comp';
end

% Save the data.
if cross_temp == 1
    crtmp_str = 'CROSSTEMPORAL_';
    name_data = [crtmp_str,area,astr,'_trials_',num2str(min_trials),'_step_',...
                    num2str(time_step),'_neurons_',num2str(num_blocks),'_pos_',...
                    num2str(num_var),'_',task,'_'];
    data_to_save = strcat(name_data,filename);
else
    name_data = [area,astr,'_trials_',num2str(min_trials),'_step_',num2str(time_step),...
                    '_neurons_',num2str(num_blocks),'_pos_',num2str(num_var),'_',...
                    task,'_'];
    data_to_save = strcat(name_data,filename);
end


if strcmpi(analysis_type,'single')
    save(data_to_save, 'all_accuracies', 'null_accuracies','num_neur');
elseif strcmpi(analysis_type,'comparative')
    save(data_to_save, 'all_accuracies', 'all_features', 'num_neur');
end

% Print progress and release memory.
fprintf('<strong>Saved the data:</strong> %s\n', data_to_save);
clear;

%% 7. Load saved data and perform permutation tests

% Start by selectively loading the saved data.
% Depending on what data are loaded, create the appropriate variables for
% the "perm" function.
% Slso, load the number of neurons used, specify if the permutations test
% should be one- or two-sided, and define some additional variables as in
% SECTION 2.

dat = 11;

if dat == 11
	% Directory where data are saved.
	savepath = '<TBD: abs_path where generated data are stored>';
    % Example filename.
	filename = 'brain_area_1_sngl_trials_25_neurons_300_num_var_4_TBD: something.mat';
    brain_area_1_condition_1 = load(strcat(savepath,filename));
    % Access generated accuracies variables.
    acc1 = brain_area_1_cond_1_cue.all_accuracies;
    acc2 = brain_area_1_cond_1_cue.null_accuracies;
    % Dedinition of extra variables, just for purposes of creating
    % informative plots.
    num_blocks = [brain_area_1_var_1_cue.num_neur];
    analysis_type = 'one';
    area = 'brain_area_1';
    center = '<TBD: e.g. cue_presentation>';
    task = '<TBD: experimental_task_1>';
    time_step = 10;
    init_time = 101;
    end_time = 1301;
    min_trials = 25;
elseif dat == 12
    % Directory where data are saved.
	savepath = '<TBD: abs_path where generated data are stored>';
    % Example filename.
	filename = 'brain_area_1_sngl_trials_25_neurons_276_num_var_4_TBD: something.mat';
    brain_area_1_condition_2 = load(strcat(savepath,filename));
    % Access generated accuracies variables.
    acc1 = brain_area_1_cond_2_cue.all_accuracies;
    acc2 = brain_area_1_cond_2_cue.null_accuracies;
    % and so on...
    
% Now, what if you want to compare say two conidtions, or two brain regions
% on the same condition?

elseif dat == 31
    % Brain area 1, condition 1 vs brain area 1, var 2.
    % Directory where data are saved.
	savepath = '<TBD: abs_path where generated data are stored>';
    % Example filenames.
	filename1 = 'brain_area_1_sngl_trials_25_neurons_300_num_var_4_TBD: something.mat';
	filename2 = 'brain_area_1_sngl_trials_25_neurons_276_num_var_4_TBD: something.mat';
    % Access generated accuracies variables.
    brain_area_1_condition_1 = load(strcat(savepath,filename1));
    brain_area_1_condition_2 = load(strcat(savepath,filename2));
    acc1 = brain_area_1_condition_1.all_accuracies;
    acc2 = brain_area_1_condition_2.all_accuracies;
    % Dedinition of extra variables, just for purposes of creating
    % informative plots.
    num_blocks = [brain_area_1_condition_1.num_neur, brain_area_1_condition_2.num_neur];
    analysis_type = 'two';
    area = 'brain_area_1';
    center = '<TBD: e.g. cue_presentation>';
    task = '<TBD: experimental_task_1_condition_1_vs_2>';
    time_step = 10;
    init_time = 101;
    end_time = 1301;
    min_trials = 25;
elseif dat == 31
    % and so on...
end

% Define the number of permutations.
n_perm = 1000;

% Keep only some of the accuracies.
subset = randperm(500,50).';

% Perform permutations.
if strcmpi(analysis_type,'one')
    [p_values] = wilcoxon(acc1(subset,:), acc2(subset,:), analysis_type);
elseif strcmpi(analysis_type,'two')
    [p_values,obs_diff,null_dist] = perm(acc1(subset,:), acc2(subset,:), n_perm, analysis_type);
    %[p_values] = wilcoxon(acc1(subset,:), acc2(subset,:), analysis_type);
end

%% 8. Plot Accuracy of classifier in each time step (run section 7 first)

% Define some unviversal variables for the "plot_accuracy" function (see
% section 2), beacuse we have choosen to do this analysis; change if
% different number has been used.
num_var = 4;
time_window = 100;

% Set p_cutoffs and mean accuracies to be ploted.
p_cutoff = [0.05, 0.01, 0.001];
mean_accuracies = vertcat(mean(acc1),mean(acc2));
sem_accuracies = vertcat(std(acc1),std(acc2));

% Plot the results
plot_accuracy(mean_accuracies, sem_accuracies, p_values, p_cutoff, analysis_type,...
            area, center, task, time_window, time_step, init_time, end_time,...
            min_trials, num_var, num_blocks);

%% 9. Load and plot cross-temporal decoding data 

% Directory where data are saved.
savepath = '<TBD: abs_path where generated data are stored>';

% Choice of analysis type: either compare "real" accuracy with estimated
% chance, or for "real" accuracies compare bins with the diagonal.
%analysis_type = 'chance';
%analysis_type = 'diagonal';
analysis_type = 'both';

% Choice of data to be loaded.
dat = 11;

if dat == 11
    % brain_area_1, task_1, condition_1
    filename = 'CROSSTEMPORAL_brain_area_1_sngl_trials_25_neurons_300_num_var_4_TBD: something.mat';
    alldata = load(strcat(savepath,filename));
	area = 'brain_area_1';
    center = 'cue';
	task = 'task_1_condition_1';
	time_step = 10;
    init_time = 101;
    end_time = 1301;
elseif dat == 12
    % and so on for other areas and/or conditions...
elseif dat == 21
	% brain_area_1, task_1, condition_1 vs condition_2, precure
    % Note that in this case the choice of "analysis type" does not matter.
    filename1 = 'CROSSTEMPORAL_brain_area_1_sngl_trials_25_neurons_300_num_var_4_TBD: something.mat';
    filename2 = 'CROSSTEMPORAL_brain_area_1_sngl_trials_25_neurons_276_num_var_4_TBD: something.mat';
	alldata1 = load(strcat(savepath,filename1));
	alldata2 = load(strcat(savepath,filename2));
	area = 'brain_area_1';
    center = 'cue';
	task = 'task_1_condition_1_vs_condition_2';
	time_step = 10;
    init_time = 101;
    end_time = 1301;
elseif dat == 22
    % and so on for other areas and/or conditions taken together...
end

% Different ways to perform the analysis based on whether you compare a
% single area/condition or not.
if dat >= 21
	% Retrieve both "real" distributions.
	acc1 = alldata1.all_accuracies;
	acc2 = alldata2.all_accuracies;
    % Retrieve both "chance" distributions.
	chance1 = alldata1.null_accuracies;
    chance2 = alldata2.null_accuracies;
	% Perfrom permutations
    % condition 1
	[p_values_chance_sp] = perm_ct_chance_diag(acc1, chance1, 'chance');
    [p_values_diag_sp] = perm_ct_chance_diag(acc1, acc1, 'diagonal');
    % condition 2
    [p_values_chance_cl] = perm_ct_chance_diag(acc2, chance2, 'chance');
    [p_values_diag_cl] = perm_ct_chance_diag(acc2, acc2, 'diagonal');
	% p-values
	p_values_sp = {p_values_chance_sp, p_values_diag_sp};
	p_values_cl = {p_values_chance_cl, p_values_diag_cl};
else
    if strcmpi(analysis_type, 'chance')
        % 1st type analysis: "real" vs chance
        acc = alldata.all_accuracies;
		chance = alldata.null_accuracies;
        % perfrom permutations
        [p_values] = perm_ct_chance_diag(acc, chance, analysis_type);
    elseif strcmpi(analysis_type,'diagonal')
        % 2nd type analysis: "real" vs diagonal - the "real" diagonal is
        % named "chance" for consistency purposes
        acc = alldata.all_accuracies;
        chance = alldata.all_accuracies;
        % perfrom permutations
        [p_values] = perm_ct_chance_diag(acc, chance, analysis_type);
    elseif strcmpi(analysis_type,'both')
        % different file for "real" and chance accuracy estimation
        acc = alldata.all_accuracies;
        chance = alldata.null_accuracies;
        [p_values_chance] = perm_ct_chance_diag(acc, chance, 'chance');
        [p_values_diag] = perm_ct_chance_diag(acc, acc, 'diagonal');
        p_values = {p_values_chance, p_values_diag};
    end
end

%% 10. Plot Accuracy of classifier in each time step (run section 9 first)

% Set p_cutoffs ('alpha').
p_cutoff = 0.001;

% Plots generation.
if dat >= 21
	% 'alpha' for 2D train-number of test points plot
    p_cutoffs = [0.001, 0.01, 0.05];
	% train-test plots of chance and diagonal together
    plot_accuracy_ct(acc1, p_values_sp, p_cutoff, area, 'condition_1', analysis_type);
	plot_accuracy_ct(acc2, p_values_cl, p_cutoff, area, 'condition_2', analysis_type);
	% train-number of test points plot
    plot_decode_gen(p_values_chance_sp, p_values_chance_cl, p_values_diag_sp,...
                        p_values_diag_cl, area, task, p_cutoffs);
else
	plot_accuracy_ct(acc, p_values, p_cutoff, area, task, analysis_type);
end
