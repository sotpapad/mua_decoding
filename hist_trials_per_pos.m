% Create histograms of number of trials per condition variation.
% This function is generic and should work using any raw-spikes data file
% irrespectively of the task.

% Inputs:
%       fullpath: data that are loaded each time
%       area: area from which data are retrieved

% Outputs:
%       hist: histogram of trials distribution per targer position

function [] = hist_trials_per_pos(fullpath, area)

    % Load the data and select the area that you are interested in.
    [rec_data, last_col] = select_area(fullpath, area);
    
    % Extract the attention positions of the task.
    % Let's assume up to 4 variations of the specific task and condition.
    if last_col == 4
        var1 = rec_data(:,last_col-3);
        var2 = rec_data(:,last_col-2);
        var3 = rec_data(:,last_col-1);
    elseif last_col == 5
        var1 = rec_data(:,last_col-4);
        var2 = rec_data(:,last_col-3);
        var3 = rec_data(:,last_col-2);
        var4 = rec_data(:,last_col-1);
    end
    
    % Extract the number of trials that have been recorded for each
    % variation.
    trials1 = zeros(size(var1,1),1);
    trials2 = zeros(size(var2,1),1);
    trials3 = zeros(size(var3,1),1);
    if last_col == 5 
        trials4 = zeros(size(var4,1),1);
    end

    % For ease of implementation just perform a for loop for each
    % variation.
    for n=1:length(trials1)
        trials1(n,1) = size(var1{n,1},1);
    end
    for n=1:length(trials2)
        trials2(n,1) = size(var2{n,1},1);
    end
    for n=1:length(trials3)
        trials3(n,1) = size(var3{n,1},1);
    end
    if last_col == 5
        for n=1:length(trials4)
            trials4(n,1) = size(var4{n,1},1);
        end
    end
    
    % Plot all the histograms in the same figure.
    figure;
    
    % Start by checking the number of variations used.
    if last_col == 4
        % Select the maximum value found in the trials variables and bin all
        % histograms accordingly.
        bins = max([max(trials1),max(trials2),max(trials3)]);
        edges = [0:1:bins];
        % Create names for the subplots titles.
        sbplt_tlt = {', Var_1', ', Var_2', ', Var_3'};
    elseif last_col == 5
        % Select the maximum value found in the trials variables and bin all
        % histograms accordingly.
        bins = max([max(trials1),max(trials2),max(trials3),max(trials4)]);
        edges = [0:1:bins];
        % Create names for the subplots titles.
        sbplt_tlt = {', Var_1', ', Var_2', ', Var_3', ', Var_4'};
    end
    
    % Variation 1 corresponds to south-west position.
    subplot(2,2,3);
    histogram(trials1,edges); ylim([0 70]);
    xlabel('Number of trials'); ylabel('Count'); title(strcat(area,sbplt_tlt{1}));
    % Variation 2 corresponds to south-east position.
    subplot(2,2,4);
    histogram(trials2,edges); ylim([0 70]);
    xlabel('Number of trials'); ylabel('Count'); title(strcat(area,sbplt_tlt{2}));
    % Variation 3 corresponds to north-east position.
    subplot(2,2,2);
    histogram(trials3,edges); ylim([0 70]);
    xlabel('Number of trials'); ylabel('Count'); title(strcat(area,sbplt_tlt{3}));
    % Variation 4 corresponds to north-west position.
    if last_col == 5
        subplot(2,2,1);
        histogram(trials4,edges); ylim([0 70]);
        xlabel('Number of trials'); ylabel('Count'); title(strcat(area,sbplt_tlt{4}));
    end
    
end