% Function that evaluates the statistical significance level of permutation
% test and creates custom labels for plotting this level.

% Inputs:
%       sign_t: array that stores statistically significant time steps
%       time: whole time of analysis
%       t: current time step
%       p_val: p-values returned from "perm.m"
%       p_cutoff: hyperparameter containing 3 statisticall significance
%                 levels

% Outputs:
        % sign_t

function [sign_t] = st_level(sign_t,time,t,p_val,p_cutoff)

    % Statistically significant results are considered only those that are
    % consecutive; here I choose 5 consecutive time steps as the minimum
    % number required for labeling a time step as significant, if of course
    % its p-value is below the p_cutoff.
    
    % for t==1 look only forward
    if t == 1
        if p_val(t:t+4) < p_cutoff(3)
            sign_t(1,t) = 3;
        elseif p_val(t:t+4) < p_cutoff(2)
            sign_t(1,t) = 2;
        elseif p_val(t:t+4) < p_cutoff(1)
            sign_t(1,t) = 1;
        end
    % for t==2 there exists 1 backward time step
    elseif t == 2
        if p_val(t-1:t+3) < p_cutoff(3)
            sign_t(1,t) = 3;
        elseif p_val(t-1:t+3) < p_cutoff(2)
            sign_t(1,t) = 2;
        elseif p_val(t-1:t+3) < p_cutoff(1)
            sign_t(1,t) = 1;
        end
    % for t==(end-1) there exists only 1 forward time step
    elseif t == length(time)-1
        if p_val(t-3:t+1) < p_cutoff(3)
            sign_t(1,t) = 3;
        elseif p_val(t-3:t+1) < p_cutoff(2)
            sign_t(1,t) = 2;
        elseif p_val(t-3:t+1) < p_cutoff(1)
            sign_t(1,t) = 1;
        end
    % for t==end look only backward
    elseif t == length(time)
        if p_val(t-4:t) < p_cutoff(3)
            sign_t(1,t) = 3;
        elseif p_val(t-4:t) < p_cutoff(2)
            sign_t(1,t) = 2;
        elseif p_val(t-4:t) < p_cutoff(1)
            sign_t(1,t) = 1;
        end
    % all other ts
    else
        if p_val(t-2:t+2) < p_cutoff(3)
            sign_t(1,t) = 3;
        elseif p_val(t-2:t+2) < p_cutoff(2)
            sign_t(1,t) = 2;
        elseif p_val(t-2:t+2) < p_cutoff(1)
            sign_t(1,t) = 1;
        end
    end

end