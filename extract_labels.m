% Function that creates the labels vector for the classifier.

% Inputs:
%       variations: number of variations of the experimental task
%       min_trials: number of minimum trials that a block must have in
%                   order for us to have a good samples-to-features ratio

% Outputs:
%       labels: vector of labels of condition variation for every trial

function [labels] = extract_labels(variations, min_trials)

    % create a labels vector
    labels = zeros(1,variations*min_trials);

    if variations == 4
        % 4 variations correspond to repeating labels 1:4
        for r=1:variations:variations*min_trials
            labels(1,r:r+3) = 1:4;
        end
    elseif variations == 3
        % 3 variations correspond to repeating labels 1,2,4
        % (making the same example assumption as in "extract_fearures.m")
        for r=1:variations:variations*min_trials
            labels(1,r:r+2) = [1,2,4];
        end
    elseif variations == 2
        % 2 variations correspond to repeating labels 2,4
		for r=1:variations:variations*min_trials
            labels(1,r:r+1) = 2:2:4;
        end
    end

end