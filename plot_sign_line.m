% Function that plots a horizontal line above the graph that indicates the
% statistically significant difference between the two conditions and/or
% areas.

% Inputs:
%        time: points of the x-axis
%        analysis_type: type of comparative analysiss ('one' == data vs chance,
%                       'two' == condition1 vs condition2)
%        data: data for which the line is plotted - cell is expected

% Outputs:
%        line: horizontal line in a preexisting graph

function [] = plot_sign_line(time, analysis_type, data)

    % Illustrate statistical significance.
    if strcmpi(analysis_type,'one')
        sign_t = data{1,1};
        hold on;
        % The last indexes cannot be parsed.
        for t=1:length(sign_t)-1
            % Use a blue horizontal line above the data, for the time
            % points that are stastically significant.
            if sign_t(1,t)==1
                plot([time(t) time(t+1)], [105 105], 'b', 'Linewidth', 1);
            elseif sign_t(1,t)==2
                plot([time(t) time(t+1)], [105 105], 'b', 'Linewidth', 2);
            elseif sign_t(1,t)==3
                plot([time(t) time(t+1)], [105 105], 'b', 'Linewidth', 3);
            end
        end
        
     elseif strcmpi(analysis_type,'two') 
        % Plot a horizontal line above the data, that indicates when there
        % exists a statistically significant difference between the two
        % conditions and/or areas.
        hold on;
        sign_t = data{1,1};
        sign2_t = data{1,2};
        % The last index cannot be parsed.
        for t=1:length(sign_t)-1
            % Blue in case condition1 > condition2.
            if sign_t(1,t)==1
                plot([time(t) time(t+1)], [105 105], 'b', 'Linewidth', 1);
            elseif sign_t(1,t)==2
                plot([time(t) time(t+1)], [105 105], 'b', 'Linewidth', 2);
            elseif sign_t(1,t)==3
                plot([time(t) time(t+1)], [105 105], 'b', 'Linewidth', 3);
            end
            % Red in case condition1 < condition2.
            if sign2_t(1,t)==1
                plot([time(t) time(t+1)], [105 105], 'r', 'Linewidth', 1);
            elseif sign2_t(1,t)==2
                plot([time(t) time(t+1)], [105 105], 'r', 'Linewidth', 2);
            elseif sign2_t(1,t)==3
                plot([time(t) time(t+1)], [105 105], 'r', 'Linewidth', 3);
            end
        end

    end
end