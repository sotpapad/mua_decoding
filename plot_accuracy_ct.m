% Function that plots cros-temporal decoding results.

% Inputs:
%		 acc: "real" accuracies of data
%		 p_values: p_values obtain by permutations testing {chance,diagonal}
%		 p_cutoffs: cut-off values for statistical significance depiction
%		 area: area where the data come from
%		 task: task
%        analysis_type: type of permutations to perform ("chance" or "diagonal")

% Outputs:
%		 plot of accuracies, training time is on x-axis and test time on
%		 the y-axis

function [] = plot_accuracy_ct(acc, p_values, p_cutoff, area, task,...
                                analysis_type)
	
	% 2D plot
	figure;
	imagesc(mean(acc,3));
    
	% Invert the Y axis.
	set(gca,'YDir','normal');
	
	% Colorbar.
	c = colorbar;
    c.Label.String = 'Accuracy (%)';

	% Contour of statistical significance.
 	colors_inv = ['r', 'm', 'g'];

	% Instead of creating contour around non-significant values do it for
	% the significant ones.
    if strcmp(analysis_type,'diagonal')
        p_cutoff = 1 - p_cutoff;
        a = ones(size(acc,1),size(acc,2));
    elseif strcmp(analysis_type,'both')
        new_p_cutoffs = 1 - p_cutoff;
        a = ones(size(acc,1),size(acc,2));
        
		% extract diagonal values for chance
		diag_chance = nan(1,size(p_values{1,1},1));
        for tr=1:size(p_values{1,1},2)
			diag_chance(1,tr) = p_values{1,1}(tr,tr);
        end
        
		% check that the diagonal is considered only when both it and the
		% corresponding points are above chance
		init_p_values_diag = p_values{1,2};
        if ~all(diag_chance <= p_cutoff)
			for tr=1:size(p_values{1,1},2)
				for ts=1:size(p_values{1,1},1)
					if ~(p_values{1,1}(tr,tr) <= p_cutoff) || ~(p_values{1,1}(ts,tr) <= p_cutoff)
						p_values{1,2}(ts,tr) = 0.0;
					end
				end
			end
        end
    end
    
    % Contours creation.
	for i=1:length(p_cutoff)
		hold on;
        if strcmp(analysis_type,'chance')
            contour(p_values,[p_cutoff(i) p_cutoff(i)],'Linewidth',1,'LineColor',...
                        'w');
        elseif strcmp(analysis_type,'diagonal')
            contour(a-p_values,[p_cutoff(i) p_cutoff(i)],'Linewidth',1,'LineColor',...
                        colors_inv(i));
        elseif strcmp(analysis_type,'both')
            % 2 p-values matrices exist
            contour(p_values{1},[p_cutoff(i) p_cutoff(i)],'Linewidth',1,'LineColor',...
                        'w');
            hold on;
			if all(diag_chance <= p_cutoff)
				contour(a-init_p_values_diag,[new_p_cutoffs(i) new_p_cutoffs(i)],...
							'Linewidth', 1 , 'LineColor', colors_inv(i));
			else
				contour(a-p_values{1,2},[new_p_cutoffs(i) new_p_cutoffs(i)],...
							'Linewidth', 1 , 'LineColor', colors_inv(i));
			end
        end
	end
	
	% Plot the onset of another experimental period of interest.
	period = 90;    % example time point
	hold on;
	plot([period period], [0 size(p_values{1},1)], '--k', 'LineWidth', 1.5);
	plot([0 size(p_values{1},1)], [period period], '--k', 'LineWidth', 1.5);
	
	% Labels.
	xlabel('Training time from cue onset (s)')
	ylabel('Test time from cue onset (s)')
	
	% Ticklabels:
	% the data are from 101 (0 ms) to 1301 (1200 ms) - step is 100 ms.
	ax = gca;
	set(ax, 'xtick', [1:20:111]);
    set(ax, 'xticklabels', [0.0+0.05:0.20:1.1+0.05]);
	set(ax, 'ytick', [1:20:111]);
    set(ax, 'yticklabels', [0.0+0.05:0.20:1.1+0.05]);

	% Title.
	title( strcat( strcat(area,':',' '), task) );

end