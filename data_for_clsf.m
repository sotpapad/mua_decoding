% Function that rearranges the experimental data structures, in a way that
% can be fed to a classifier

% Inputs:
%       features: blocks of trials from a specific area, with at least
%                 'min_trials' number of trials
%       min_trials: number of minimum trials that a block must have in
%                   order for us to have a good samples-to-features ratio
%       time_window: duration of sliding time window (in msec) that is
%                    used for training a classifier - it controls the time
%                    evolution of the decoding analysis in "apply_time_window"
%       time_step: amount of time points that the windows slides (in msec)
%       init_time: initial time point for time window application
%       num_windows: number of windows that feet into the whole duration of
%                    a trial

% Outputs:
%       time_evolution_counts: rearranged data, ready to be fed to a classifier
%                              (neurons-features x trials-samples) - spikes
%                              counts in each time window

function [time_evolution_counts] = data_for_clsf(features, min_trials,...
				time_window, time_step, init_time, num_windows)
    
	% The variable positions contains the number of positions of the
	% experimental task
	variations = size(features,2);
	
	% We need the prior probabilities of each sample to be the same; the
	% "extract_features" algorithm has not made sure of this; to accomplish
	% this, randomly select min_trials number of trials from each variation
	% of each feature.
	for f=1:size(features,1)
		for p=1:variations
			% randomly select min_trials number of trials, without replacement
			t = randperm(size(features{f,p},1),min_trials);
			features{f,p} = features{f,p}(t,:);
		end
	end

	% The data should be rearranged in a matrix of dimensions:
	% [features x (variations x min_trials)]
    % Now instead of blocks of trials we have "neurons".
	patterns = cell(size(features,1), (variations * min_trials));

	% Select row from features.
	for i=1:size(features,1)
		% Select column from features.
		for j=1:variations
			% Insert it in the correct patterns column.
			timeseries = features{i,j};
			for tms=1:size(timeseries,1)
				patterns{i,j+(variations*(tms-1))} = timeseries(tms,:);
			end
		end
	end

	% The final step is to create a 3D cell array, where the pages are created
	% by applying the sliding window over the "patterns" and extracting the
	% spike count at each point.
	[time_evolution_counts] = apply_time_window(patterns, time_window, time_step, ...
											init_time, num_windows);
											
end