% Function that selects the data coming from only one area and having a
% minimum number of trials from the experimental data structures.

% Inputs:
%       fullpath: full path to the data that are loaded each time
%       area: area from which data are retrieved
%       num_var: number of variations to used in the analysis depicted
%       min_trials: number of minimum trials that a probe must have in
%                   order for us to have a good samples-to-features ratio
%       analysis_type: if 'comparative' it means that only a subset of xxx
%                      neurons are to be used, so that data with equal
%                      number of probes for different experimental
%                      conditions are generated
%       subset: number of probes used if "analysis_type" == "comparative"

% Outputs:
%       features: blocks of trials from a specific area, with at least
%                 'min_trials' number of trials
%       labels: vector of labels of condition variation for every trial

function [features, labels] = extract_features(fullpath, area, num_var,...
												min_trials, analysis_type, subset)

    % Load the data and select the area that you are interested in.
    % Specifically, for each experimental task and condition, you (may)
    % have trials that reflect variations (e.g. appearence of target of
    % attention or movement to a different part of the space).
    [rec_data, last_col] = select_area(fullpath, area);
    
    % Select the number of positions to be kept, depending on the
    % experimental condition at hand.
    if num_var <= 1
        msg = ['You cannot run a comparative analysis of 4 variations with just 1!',...
                '\nCheck the "num_var" variable'];
        error(msg,num_var);
    end
    
    % For example, from section 3 or prior knowledge you have determined
    % that certain variations are of most interest.
    if last_col == 5
		if num_var == 2
            % for 2 variations, we imply that we want to compare variations
            % 2 and 4
            rec_data = [rec_data(:,2) rec_data(:,4)];
        elseif num_var == 3
            % for 3 variations, we imply that we want to compare variations
            % 1, 2 and 4
            rec_data = [rec_data(:,1) rec_data(:,2) rec_data(:,4)];
		end		
    end
    
    % Keep the blocks of trials that contain at least 'min_trials' number
    % of trials.
    blocks_with_min_trials = zeros(size(rec_data,1),1);
    for n=1:size(rec_data,1)
        % Case all variations are selected.
        if num_var == 4
            if size(rec_data{n,1},1) >= min_trials && ...
                    size(rec_data{n,2},1) >= min_trials && ...
                    size(rec_data{n,3},1) >= min_trials && ...
                    size(rec_data{n,4},1) >= min_trials
               blocks_with_min_trials(n,1) = 1;
            end
        elseif num_var == 3
            % Case 3 variations are selected.
            if size(rec_data{n,1},1) >= min_trials && ...
                    size(rec_data{n,2},1) >= min_trials && ...
                    size(rec_data{n,3},1) >= min_trials
               blocks_with_min_trials(n,1) = 1;
            end
        elseif num_var == 2
            % Case 2 variations are selected.
            if size(rec_data{n,1},1) >= min_trials && ...
                    size(rec_data{n,2},1) >= min_trials
               blocks_with_min_trials(n,1) = 1;
            end
        end
    end
    blocks_with_min_trials = logical(blocks_with_min_trials);
    
    % The variable "features" contains all the blocks with at least
    % 'min_trials' number of trials.
    features = rec_data(blocks_with_min_trials,:);
        
    % Select if all the features or just a subset of them should be kept.
    if strcmpi(analysis_type,'comparative')
        to_use = randperm(size(features,1),subset);
        features = features(to_use,:);
    end

    % The variable "variations" contains the number of variations of the
    % experimental task and condition.
    variations = size(features,2);

    % create an appropriate labels array
    [labels] = extract_labels(variations, min_trials);
    
end